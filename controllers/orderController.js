const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");



const getTotalAmount = async (products) => {
	let totalCost = 0;
	console.log(products)
	return await Promise.all(products.map(product => {
		return Product.findById(product.productId)
		.then(result => {
			let cost = result.price * product.quantity;
			console.log("cost: " + cost);
			return cost;
		})
	}))
}

// const updateTotalAmount = async (userData) => {
// 	return Order.find({userId: userData.id}).then(results => {
// 		results.map(result => {
// 			return getTotalAmount(result.products).then(arr => {
// 				let totalCost = arr.reduce((x,y) => x + y);
// 				// console.log("Total is " + totalCost);
// 				// console.log(arr);
// 				// console.log(result);
// 				result.totalAmount = updatedTotalCost;

// 			});
// 		})
// 	});
// }


// Checkout / Create Order (User only)
module.exports.addOrder = (reqBody, data) => {
	if (!data.isAdmin) {
		// calculate total amount based on products and their respective quantities
		let products = reqBody.products;
		return getTotalAmount(products).then(arr => {
			let totalCost = arr.reduce((x,y) => x + y);
			console.log("Total is " + totalCost);
			let newOrder = new Order ({
				userId: data.userId,
				products: products,
				totalAmount: totalCost
			});
			return newOrder.save().then((order, error) => {
				return error ? false : {orderId: order._id};
			});
		});
		// Product.findById("63e1be9f1d9317c836d7a00e").then(res => console.log(res.price));


		// // let newOrder = {
		// // 	userId: data.userId,
		// // 	products: reqBody.products,
		// // 	// calculated
		// // 	totalAmount: reqBody.totalAmount
		// // }
		// let message = Promise.resolve("OK");
		// return message.then(value => {
		// 	return {value};
		// });

	} else {
		let message = Promise.resolve("Only USER can access this");
		return message.then(value => {
			return {value};
		});
	}
}

/*
	{
	    "products": [
	        {
	            "productId": "",
	            "quantity": 2
	        }
	    ]
	}
*/


// Retrieve authenticated user’s orders (self)
module.exports.getOrders = (userData) => {
	return Order.find({userId: userData.id}).then(result => {
		return result;
	});
}

// Retrieve an order of a user
module.exports.getOrder = (reqParams) => {
	return Order.findById(reqParams.orderId).then(result => {
		return result;
	});
}


// Retrieve all orders (Admin only)
module.exports.getAllOrders = (isAdmin) => {
	if (isAdmin) {
		return Order.find({}).then(result => {
			return result;
		});
	} else {
		let message = Promise.resolve("Only ADMIN can access this");
		return message.then(value => {
			return {value};
		});
	}
}


/*
	Add to cart
	- Added Products
	- Change product quantities
	- Remove products from cart
	- Subtotal for each item
	- Total price for all items
*/

// Add products to order
module.exports.addProductsToOrder = (reqParams, reqBody) => {
	return Order.findById(reqParams.orderId).then(order => {
		if (order) {
			order.products.push(...reqBody.productsToAdd);
			// re-calculate total amount
			let updatedProducts = order.products;
			return getTotalAmount(updatedProducts).then(arr => {
				let updatedTotalCost = arr.reduce((x,y) => x + y);
				order.totalAmount = updatedTotalCost;
				return order.save().then((saveOrder, error) => {
					return error ? false : true;
				});
			});
		} else {
			let message = Promise.resolve("Order not found");
			return message.then(value => {
				return {value};
			});
		}
	})
}


// Change quantity of a product in an order (Remove products from order)
module.exports.updateQuantity = (reqParams, reqBody) => {
	return Order.findById(reqParams.orderId).then(order => {
		if (order) {
			// Find the product we want in the products array
			for (let i = 0; i < order.products.length; i++) {
				if (order.products[i].productId == reqParams.productId) {
					order.products[i].quantity = reqBody.quantity;
					// recalculate total amount
					return getTotalAmount(order.products).then(arr => {
						let updatedTotalCost = arr.reduce((x,y) => x + y);
						order.totalAmount = updatedTotalCost;
						// remove product if quantity was set to zero
						if (reqBody.quantity == 0) {
							order.products.splice(i, 1);
						}
						return order.save().then((saveOrder, error) => {
							return error ? false : true;
						});
					});
				}
			}

			return "Product not found";
			// let isProductExisting = order.products.some(product => {
			// 	return product.productId == reqParams.productId;
			// })
			// if (isProductExisting) {
			// 	return "I AM FOUND"
			// } else {
			// 	let message = Promise.resolve("Product not found");
			// 	return message.then(value => {
			// 		return {value};
			// 	});
			// }
			// re-calculate total amount
			// let updatedProducts = order.products;
			// return getTotalAmount(updatedProducts).then(arr => {
			// 	let updatedTotalCost = arr.reduce((x,y) => x + y);
			// 	order.totalAmount = updatedTotalCost;
			// 	return order.save().then((saveOrder, error) => {
			// 		return error ? false : true;
			// 	});
			// });
		} else {
			let message = Promise.resolve("Order not found");
			return message.then(value => {
				return {value};
			});
		}
	})
}


// Get Subtotal for each product in an order
module.exports.getSubtotal = (reqParams) => {
	return Order.findById(reqParams.orderId).then(order => {
		return Promise.all(order.products.map(product => {
			return Product.findById(product.productId)
			.then(result => {
				let cost = result.price * product.quantity;
				return {name: result.name, price: result.price, quantity: product.quantity, subtotal: cost, _id: product._id};
			})
		}))
	});
}


// Get total amount in an order
module.exports.getTotalAmount = (reqParams) => {
	return Order.findById(reqParams.orderId).then(order => {
		return order ? {totalAmount: order.totalAmount} : {message: "Order not found"};
	});
}