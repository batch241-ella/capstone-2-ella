const Product = require("../models/Product");
const auth = require("../auth");


// Create a product (Admin only)
module.exports.addProduct = (data) => {

	if (data.isAdmin) {
		let newProduct = new Product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		});

		return newProduct.save().then((product, error) => {
			return error ? false : product;
		});
	} else {
		let message = Promise.resolve("User must be ADMIN to access this");
		return message.then(value => {
			return {value};
		});
	}

}


// Retrieve all the products
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	});
}


// Retrieve all the active products
module.exports.getActiveProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	});
}


// Retrieve single product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
}


// Update product informtaion (Admin only)
module.exports.updateProduct = (reqParams, data) => {
	if (data.isAdmin) {
		let updateProduct = {
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		};
		// Object.entries(updateProduct).forEach(([key, value]) => {
		// 	console.log("value is: " + value);
		// 	if (!value) {
		// 		console.log("TRIGGERED");
		// 		return Promise.resolve("ERROR");
		// 	}
		// 	console.log("...");
		// });
		return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((product, error) => {
			return error ? false : true;
		});

	} else {
		let message = Promise.resolve("User must be ADMIN to access this");
		return message.then(value => {
			return {value};
		});
	}
}
	


// Archive Product (Admin only)
module.exports.archiveProduct = (reqParams, reqBody, data) => {
	console.log(data);
	if (data.isAdmin) {	
		return Product.findByIdAndUpdate(reqParams.productId, {isActive: reqBody.isActive}).then((product, error) => {
			return error ? false : true;
		});
	} else {
		let message = Promise.resolve("User must be ADMIN to access this");
		return message.then(value => {
			return {value};
		});
	}
}