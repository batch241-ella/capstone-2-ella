const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");




// Checking if the email exists in the database
module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => {
		return result.length > 0;
	});

}

// User registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		// bcrypt.hashSync(<dataToBeHashed>, <saltRound>)
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then((user, error) => {
		return error ? false : user;
	});	
}


// User Authentication
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			return isPasswordCorrect ? { access: auth.createAccessToken(result) } : false;
		}
	});
}


// Retrieve User Details (self)
module.exports.getUserInfo = (userData) => {
	return User.findById(userData.userId).then(user => {
		return user ? user : false;
	});
}


// Set user as admin (Admin only)
module.exports.setAdmin = (reqParams, data) => {
	if (data.isAdmin) {
		return User.findByIdAndUpdate(reqParams.userId, {isAdmin: true}).then((result, error) => {
			return error ? false : true;
		});
	} else {
		let message = Promise.resolve("User must be ADMIN to access this");
		return message.then(value => {
			return {value};
		});
	}
}

// Retrieve All User Details (Admin Only)
module.exports.getAllUsersInfo = (isAdmin) => {
	if (isAdmin) {
		return User.find({}).then(result => {
			return result;
		});
	} else {
		let message = Promise.resolve("User must be ADMIN to access this");
		return message.then(value => {
			return {value};
		});
	}
}





