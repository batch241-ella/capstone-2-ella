const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController");
const auth = require("../auth");



// Route for creating an order / checking out (User only)
router.post("/addOrder", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization);
	const data = {
		userId: userData.id,
		isAdmin: userData.isAdmin
	}
	orderController.addOrder(req.body, data).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving authenticated user’s orders (self)
router.get("/getOrders", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization);
	orderController.getOrders(userData).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving an order of a user 
router.get("/:orderId/getOrder", auth.verify, (req, res) => {
	orderController.getOrder(req.params).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving all orders (Admin only)
router.get("/all", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	orderController.getAllOrders(isAdmin).then(resultFromController => res.send(resultFromController));
});


// ADD TO CART

// Route for adding products to order
router.post("/:orderId/addProductsToOrder", auth.verify, (req, res) => {
	orderController.addProductsToOrder(req.params, req.body).then(resultFromController => res.send(resultFromController));
});


// Route for changing quantity of a product in an order and for removing products from order
router.patch("/:orderId/:productId/updateQuantity", auth.verify, (req, res) => {
	orderController.updateQuantity(req.params, req.body).then(resultFromController => res.send(resultFromController));
});


// Route for getting the subtotal for each product in an order
router.get("/:orderId/getSubtotal", auth.verify, (req, res)=> {
	orderController.getSubtotal(req.params).then(resultFromController => res.send(resultFromController));
});


// Route for getting the total amount in an order
router.get("/:orderId/getTotalAmount", auth.verify, (req, res) => {
	orderController.getTotalAmount(req.params).then(resultFromController => res.send(resultFromController));
});



module.exports = router;