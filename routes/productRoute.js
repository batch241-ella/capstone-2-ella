const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");



// Route for creating a product (Admin only)
router.post("/addProduct", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data).then(resultFromController => {
		res.send(resultFromController);
	});

});

// Route for retrieving all ACTIVE products
router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => {
		res.send(resultFromController);
	});
});

// Route for retrieving all ACTIVE products
router.get("/all-active", (req, res) => {
	productController.getActiveProducts().then(resultFromController => {
		res.send(resultFromController);
	});
});

// Route for retrieving a single product
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => {
		res.send(resultFromController);
	});
});

// Route for updating product information (Admin only)
router.put("/:productId", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	// Check if there are empty fields [FIX]
	Object.entries(data.product).forEach(([key, value]) => {
			if (!value) {
				res.send(`${key} cannot be empty!`);
			}
		});
	productController.updateProduct(req.params, data).then(resultFromController => {
		res.send(resultFromController);
	});
});

// Route for archiving a product (Admin only)
router.patch("/:productId/archive", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.archiveProduct(req.params, req.body, data).then(resultFromController => {
		res.send(resultFromController);
	});
});



module.exports = router;