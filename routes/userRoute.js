const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");


// Route for checking if the user's email already exists in our database
router.post("/checkEmail", (req, res) => {

	userController.checkEmailExists(req.body).then(resultFromController => {
		res.send(resultFromController);
	});

});

// Route for User Registration
router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => {
		res.send(resultFromController);
	});

});


// Route for User Authentication
router.post("/login", (req, res) => {

	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));

});


// Route for retrieving user details
router.get("/details", auth.verify, (req, res) => {
	const userData = {
		userId: auth.decode(req.headers.authorization).id
	}
	userController.getUserInfo(userData).then(resultFromController => res.send(resultFromController));
});


// Route for setting user as admin (Admin only)
router.patch("/:userId/setAdmin", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.setAdmin(req.params, data).then(resultFromController => res.send(resultFromController));
});

// Route for getting all users
router.get("/all", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.getAllUsersInfo(isAdmin).then(resultFromController => res.send(resultFromController));
});



module.exports = router;